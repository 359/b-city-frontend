import * as angular from 'angular';
import '@uirouter/angularjs';
import { LoginComponent } from './components/loginComponent/LoginComponent';
import { config as routeConfig } from './configs/routes';

angular.module('app.login', ['ui.router'])
    .component('login', new LoginComponent())
    .config(routeConfig);