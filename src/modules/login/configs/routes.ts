config.$inject = ['$stateProvider'];
export function config($stateProvider: angular.ui.IStateProvider): void {

    // let stateConfig: ng.ui.IState = {
    //     url: '/login',
    //     component: 'login'
    // };

    $stateProvider.state( 'login', new LoginStateConfig() );

}

export class LoginStateConfig implements ng.ui.IState {
    public url: string = '/login';

    public component: string = 'login';


}
