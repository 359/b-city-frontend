export class LoginComponent implements ng.IComponentOptions {

    public template: string = `
        <div class="row login-form">
            <div class="col-md-4 col-md-offset-4">
                <h1 class="text-center form-heading">Blob City</h1>
                <form class="custom-form">
                    <div class="form-group">
                        <input class="form-control" type="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" placeholder="Password">
                    </div>
                    <div class="form-group"></div>
                    <div class="checkbox">
                        <label>
                        <input type="checkbox">Remember me</label>
                    </div>
                    <button class="btn btn-warning btn-block btn-sm submit-button" ui-sref="dashboard" type="button">Login </button>
                </form>
            </div>
        </div>
    `;

    public controller: any = LoginController;


}

export class LoginController {
    constructor() {
    }

}

