import * as angular from 'angular';
import '@uirouter/angularjs';

import { DashboardComponent } from './components/dashboard/DashboardComponent';
import { NavbarComponent } from './components/navbar/NavbarComponent';
import { SidebarComponent } from './components/sidebar/SidebarComponent';
import { PlusButtonComponent } from './components/plusButton/PlusButtonComponent';
import { BoardComponent } from './components/board/BoardComponent';
import { SectionComponent } from './components/section/SectionComponent';
import { ChartContainer } from './components/chartContainer/ChartContainerComponent';

import { config as routesConfig } from './configs/routes';

angular.module('app.dashboard', ['ui.router'])
    .component('dashboard', new DashboardComponent())
    .component('navbar', new NavbarComponent())
    .component('sidebar', new SidebarComponent())
    .component('plusButton', new PlusButtonComponent())
    .component('board', new BoardComponent())
    .component('section', new SectionComponent())
    .component('chartContainer', new ChartContainer())

    .config(routesConfig);