export class BoardComponent implements ng.IComponentOptions {

    public template: string = `
            <div class="board-wrapper">        
                    <div ng-repeat="section in $ctrl.sections">
                        <section size="section.size" ></section>
                    </div>
            </div>`;

    public controller: ng.IControllerConstructor = BoardController;

}



export class BoardController implements ng.IComponentController {

    public sections: Array<any> = [
        {
            size: 1

        },
        {
            size: 3
        }];

    public $onInit(): void {


    }


}