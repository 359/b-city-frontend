export class DashboardComponent implements ng.IComponentOptions {

    public template: string = `
    <div>
        <navbar></navbar>
        <sidebar></sidebar>
    </div>
    `;

}