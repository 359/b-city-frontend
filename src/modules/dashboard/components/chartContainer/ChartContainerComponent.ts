import './ChartContainer.scss';

/**
 * Chart container component.
 * 
 * @export
 * @class ChartContainerComponent
 * @implements {ng.IComponentOptions}
 */
export class ChartContainer implements ng.IComponentOptions {

    public template: string = `
        <div class="chart-container-wrapper">
            <span class=" glyphicon glyphicon-plus"></span>
        </div>
    `;

    public controller: ng.IControllerConstructor = ChartContainerController;


}



export class ChartContainerController implements ng.IComponentController {



}