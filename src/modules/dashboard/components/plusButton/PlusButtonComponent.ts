import './PlusButton.scss';

export class PlusButtonComponent implements ng.IComponentOptions {
    public template: string = `
        <div class="plus-button-wrapper">
           <span class="glyphicon glyphicon-plus center plus-button-icon"></span>     


        </div>
    `;
}