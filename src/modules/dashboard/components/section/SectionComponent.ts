import './Section.scss';

export class SectionComponent implements ng.IComponentOptions {
    public template: string = `
    
        <div class="row">

            <div ng-repeat="container in $ctrl.containersData" ng-class="$ctrl.getGridClass($ctrl.size)" >
                <chart-container></chart-container>
            </div>
            
        </div>`;

    public bindings: any = {
        size: '<'
    };

    public controller: ng.IControllerConstructor = SectionComponentController;

}

export class SectionComponentController implements ng.IComponentController {

    public size: number;
    public containersData: Array<any> = [];

    public $onInit(): void {
        console.log( 'inited a section with containersCount: ', this.size );

        this.initContainersData(this.size);

    };


    /**
     * addContainer - fires when user click plus button
     * 
     * @memberof SectionComponentController
     */
    public addContainer(): void {
        alert('add');

    }


    private getGridClass(gridsize: number): string {
        switch (gridsize) {
            case 1:
            return 'col-lg-12 col-md-12 col-sm-12';
                break;
            case 2:
            return 'col-lg-6 col-md-6 col-sm-6';
                break;
            case 3:
            return 'col-lg-4 col-md-4 col-sm-4'
                break;
            default: '';
            break;
        }

    }

    private initContainersData(length): void {
        for(let i = 0; i < length; i++ ) {
            this.containersData.push({
                empty: true
            });

        }
    }

}


