import './Navbar.scss';

export class NavbarComponent implements ng.IComponentOptions {

    public template: string =  `<nav class="navbar navbar-default navbar-bc">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
            <a class="navbar-brand" href="#">BlobCity</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Welcome, Username!</a></li>
            <li><a ui-sref="login">Logout</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        Actions
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">Action 1</a></li>
                    <li><a href="#">Action 2</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Action 3</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
    </nav>
    `;

}