import './Sidebar.scss';

export class SidebarComponent implements ng.IComponentOptions {
    public controller: any = SidebarController;
    public template: string =
    `<div id="wrapper">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li> <a href="#">Worksheet #1</a></li>
            <li> <a href="#">Customer Churn</a></li>
            <li> <a href="#">Sales Forecast</a></li>
            <li> <a href="#">opened: </a></li>
        </ul>
        <plus-button></plus-button>

    </div>
    <div class="page-content-wrapper">
        <div class="row">
            <board></board>
        </div>
    </div>
</div>`;
}
export class SidebarController {

    public open: boolean = true;

    public toggle(): void {
        this.open = !this.open;
    }

}