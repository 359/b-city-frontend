config.$inject = ['$stateProvider'];
export function config($stateProvider: angular.ui.IStateProvider): void {

    let stateConfig: ng.ui.IState = {
        url: '/',
        component: 'dashboard'
    };

    $stateProvider.state('dashboard', stateConfig)

}