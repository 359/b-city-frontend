import * as angular from 'angular';
import { RootComponent } from './components/rootComponent/RootComponent';


angular.module('app.root', [])
    .component('root', new RootComponent());    