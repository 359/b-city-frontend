export class RootComponent implements ng.IComponentOptions {

    public template: string = `<div>
                                 <ui-view></ui-view>
                               </div>`;

}