import * as angular from 'angular';
import '@uirouter/angularjs';

// TODO: decide to create index.ts in component folder or not
import './modules/root';
import './modules/login';
import './modules/dashboard';


// load our default (non specific) css
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';

import { config  as routeConfig } from './configs';


angular.module('app', ['app.root', 'app.login', 'app.dashboard'])
    .config(routeConfig);



angular.bootstrap(document, ['app'], {
    strictDi: true
});