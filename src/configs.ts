config.$inject = [ '$urlRouterProvider' ];

export function config( $urlRouterProvider: ng.ui.IUrlRouterProvider ) {

    $urlRouterProvider.otherwise('/login');

}